$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();
  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/"+sala;
  })
  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var user = $("#autor").val();
    $("#chatBox").append(`<p>${user}: ${msg}<p>`);
    var toSend = {user: user, text: msg};
    socket.emit("newMsg", toSend);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", (data) => {
    console.log(data);
    $("#chatBox").append(`<p>${data.user}: ${data.text}<p>`);
  })
});
