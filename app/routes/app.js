var express = require("express");
var path = require("path");
var router = express.Router();
var ctrlDir = "/app/controllers";

// Controllers
var alumnoCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));

router.get("/", function(req, res, next) {
    res.render("chat", {title: "Chat"});
});

router.get("/asignatura", async function(req, res, next) {
    var docente = await docenteCtrl.listarDocentes(req, res, next);
    var alumnos = await alumnoCtrl.listarAlumnos(req, res, next);
    console.log(alumnos);
    console.log(docente);
    res.render("newAsignatura", {docentes: docente, alumnos: alumnos});
});
/*
router.get("/asignatura", function(req,res,next){
    res.render("newAsignatura");
});*/
module.exports = router;