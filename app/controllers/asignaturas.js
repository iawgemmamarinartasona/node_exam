var mongoose = require("mongoose"),
  Asignatura = require("../models/asignatura");
const asignatura = require("../models/asignatura");

//listar
var listarAsignatura = (req, res, next) => {
  Asignatura.find(function (err, Asignaturas) {
    return Asignaturas;
  });
};
//crear
var crearAsignatura = (req, res, next) => {
  var asignatura = new Asignatura(req.body);
  asignatura.save((err, res) => {
    if (err) console.log(err);
    console.log("asignatura insertada");
    return res;
  });
};
//editar
var editarAsignatura = (req, res, next) => {
    
};
//eliminar
var eliminarAsignatura = (req, res, next) => {

};

module.exports = {
  listarAsignatura,
  eliminarAsignatura,
  editarAsignatura,
  crearAsignatura,
};
